<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage FreshMaid
 * @since Twenty Fifteen 1.0
 */
?><!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Booking Confirmation | So Fresh Maid</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/vendors/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/resources/css/style.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/resources/css/responsive.css">
  
  

  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->

  <!-- Favicon and Apple Icons -->
	<link rel="shortcut icon" href="">
	<link rel="apple-touch-icon" href="">
	<link rel="apple-touch-icon" size="72x72" href="">
	<link rel="apple-touch-icon" size="114x114" href="">
	<?php wp_head(); ?>
</head>

<body>
  <!-- Navigation -->
  <header class="site-header">
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  <?php 
			$custom_logo_id = get_theme_mod( 'custom_logo' );
			$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
			//echo "<pre>";print_r($image);
		  ?>
		  <a class="navbar-brand" href="#"><img src="<?php echo $image[0];?>" alt="So Fresh Maid"></a>
        </div>

        <div class="collapse navbar-collapse" id="main-menu">
		<?php wp_nav_menu( array('menu' => 'header-menu' , 'menu_class' => 'nav navbar-nav navbar-right' ));?>
          <!--<ul class="nav navbar-nav navbar-right">
            <li><a href="#">Locations</a></li>
            <li><a href="#">What We Clean</a></li>
            <li><a href="#">Help</a></li>
            <li><a href="tel:8325104165">(832) 510-4165</a></li>
            <li><a href="#">Login</a></li>
          </ul>-->
        </div>
      </div>
    </nav>
  </header>
=== Footer Logo ===
Contributors: D001928403
Tags: Footer Logo, Need Extra Logo, Another Logo For Theme, Multiple Logo 
Requires at least: 4.2
Tested up to: 4.8
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Footer Logo

== Description ==

It is add a upload capability to footer logo or another cutom logo in your website without affecting header image .
also you are able to change your theme logo without using FTP or the media library. 

Just add <?php echo get_footer_image(); ?> or <?php footer_image(); ?>  with the code from the original theme files.

== Installation ==

1. Upload the entire `footer-logo` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.



== Frequently Asked Questions ==

1. Will it affect header logo code?
No


== Screenshots ==

1. screenshot-1.png
1. screenshot-2.png

== Changelog ==

= 1.0. =

* Added a filter on the frontend input field

= 1.0.0 =

* Initial release
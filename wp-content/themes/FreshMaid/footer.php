<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage FreshMaid
 * @since FreshMaid 1.0
 */
?>
<footer class="page-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="footer-logo">
			<img src="<?php echo get_footer_image();?>" alt="So Fresh Maid">
		  </div>
		  <?php dynamic_sidebar( 'footer1' ); ?>
        </div>
		<?php dynamic_sidebar( 'footer2' ); ?>
      </div>
      <div class="bottom-bar">
        <div class="row">
          <div class="col-md-6">
            <p class="copyright">2018 All Rights Reserved. Copyright So Fresh Maids.</p>
          </div>
          <div class="col-md-6">
		  <?php wp_nav_menu( array('menu' => 'Footer-menu' , 'menu_class' => 'footer-btns pull-right' ));?>
            <!--<div class="footer-btns pull-right">
              <a href="#">Home</a>
              <a href="#">Who Are We?</a>
              <a href="#">Join the Crew</a>
            </div>-->
          </div>
        </div>
      </div>
    </div>
  </footer>
<?php wp_footer(); ?>

  <!-- Scripts -->
  <script src="<?php echo get_template_directory_uri();?>/vendors/js/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri();?>/xvendors/js/bootstrap.min.js"></script>
</body>
</html>

<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'freshmaid');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#W]53Xmo?r34d:g.]C=xJ;pwYUwtYX~D`U${F8(]EM*_;}aVvo36J+d2<zOV{y~0');
define('SECURE_AUTH_KEY',  ';Fsy%<y6j9&Kge)LruG9![vl7A,mO;+wJ$L<_%=5;>}KkC1AAe{;AM%i6r%p~toG');
define('LOGGED_IN_KEY',    'IgIcW9R4^F5),Q`J(Vg~&j,p`yq|{P^;y:sc3 mkJ`9TZ8QVFFS%:,t>q9IJ4SDK');
define('NONCE_KEY',        'CmHxE?C%|ujNh]&+[/[T)$.PnET$0bJ)E(qZ359-]fQn*Z0tqFkmS3HUX1Fg[O@^');
define('AUTH_SALT',        'wF7p+Z)AG:,/uQ~UT>7qizP8Eio8jf&wJBG*7p FJ@vQipVev|jSD[ilB]zfrMov');
define('SECURE_AUTH_SALT', 'xr7Z{!HO]S}AV=Yw<A$!kJ%2(]p|4Sc0re<i!;:gB.PT[<]>e`fI1Lbqlp1.M]X-');
define('LOGGED_IN_SALT',   'DCE@#6nBht|Q8^H S/BAl>O>9N!=b=4_lkC7/yq?%a:}u)E.|qFM(Ua1I1fC+8Va');
define('NONCE_SALT',       'dW-|[U)K)p(t%GcXj=(!^u L=IlGZzh#%n~wzn6j_:h!kqp[PGJ|n52pViDUq&1C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

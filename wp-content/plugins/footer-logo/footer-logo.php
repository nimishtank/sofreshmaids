<?php
/**
* Plugin Name: Footer Logo
* Plugin URI: https://plugins.wordpress.org/footer-logo/
* Description: You can able to manage 1 another logo in your website
* Version: 1.0
* Author: D001928403
* Contributors: D001928403
* Author URI: 
* License: GPL2
*/





if ( ! defined( 'ABSPATH' ) ) {
	exit; // Not Defined
}

if ( ! class_exists( 'Footer_Logo' ) ) :
class Footer_Logo{
	function __construct(){
		add_action( 'customize_register', array($this,'set_footer_image'));
	}
	function set_footer_image($wp_customize) {
			$wp_customize->add_section( 'ignite_custom_logo', array(
				'title'          => 'Footer Logo',
				'description'    => 'Display a footer logo?',
				'priority'       => 25,
			) );
			$wp_customize->add_setting( 'footer_custom_logo', array(
				'default'        => '',
			) );
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_custom_logo', array(
				'label'   => 'Footer logo',
				'section' => 'ignite_custom_logo',
				'settings'   => 'footer_custom_logo',
			) ) );
	}

}
new Footer_Logo;
function get_footer_image(){
	return get_theme_mod( 'footer_custom_logo' ); 
}
function footer_image(){
	echo get_footer_image();
}
endif;
<?php 
/* Template Name: home */ 
get_header(); ?>

<!-- Booking Confirmation Section -->
  <section class="page-section c-booking-section" style="background-image: url('<?php echo get_template_directory_uri();?>/resources/images/booking-confirmation-bg.png');">
    <div class="container">
		<?php if( have_rows('slider') ): ?>
			<?php while( have_rows('slider') ): the_row(); ?>
				<h1 class="section-title color-white text-center"><?php the_sub_field('content1'); ?></h1>
				<p class="section-sub-title text-center"><?php the_sub_field('content2'); ?></p>
				<div class="video-block">
					<div style="padding:56.25% 0 0 0;position:relative;">
					  <iframe src="<?php the_sub_field('addvideo'); ?>" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
					<script src="https://player.vimeo.com/api/player.js"></script>
				</div>
				<li>
					<a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('title'); ?></a>
				</li>
			<?php endwhile; ?>
		<?php endif; ?>	
    </div>
  </section>

  <!-- Nutrition Tour Section -->
  <section class="page-section n-tour-section">
    <div class="container">
      <h2 class="section-title no-subtitle text-center"><?php the_field('service_Title'); ?></h2>
      <div class="row">
		
		<?php if( have_rows('service_section') ): ?>
 
			<?php while( have_rows('service_section') ): the_row(); ?>
				<div class="col-md-3">
				  <div class="service-card text-center">
					<div class="service-img-wrapper">
					  <img src="<?php the_sub_field('serviceimag'); ?>" alt="Arrival Window">
					</div>
					<h3><?php the_sub_field('servicetitle'); ?></h3>
					<p><?php the_sub_field('servicecontent'); ?></p>
				  </div>
				  
				  
				  
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
      </div>
    </div>
  </section>

  <!-- Thanks Again Section -->
  <section class="page-section t-again-section" style="background-image: url('resources/images/thanks-again-bg.png');">
    <div class="container">
      <h2 class="section-title text-center"><?php the_field('our_team_title'); ?></h2>
      <p class="section-sub-title text-center"><?php the_field('team'); ?></p>
      <div class="row">
		<?php 
		$profileImage = get_field('team_profile_image');
		if( $profileImage ): ?>
				<?php foreach( explode("," ,$profileImage) as $image ): ?>
				<?php $gallary = get_post_meta( $image, '_wp_attached_file', true );?>
					<div class="col-md-2">
					  <img src="<?php echo get_site_url()."/wp-content/uploads/".$gallary;?>" class="t-again-img">
					</div>
				<?php endforeach; ?>
		<?php endif; ?>
      </div>
    </div>
  </section>

  <!-- Booking Section -->
  <section class="page-section booking-section">
    <div class="container">
      <h3 class="section-title text-center"><?php the_field('book'); ?></h3>
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
		<?php echo do_shortcode( '[contact-form-7 id="47" title="Book a cleaning now"]' ); ?>
        </div>
      </div>
    </div>
  </section>
  
<?php get_footer(); ?>